import traceback
from concurrent.futures.thread import ThreadPoolExecutor
import pdb
import sys
import time

currentTimeMilliseconds = lambda: int(round(time.time() * 1000))

class ControllerChangePlaybackHandler:
    
    def __init__(self):
        self.recorderToFutureDict = dict()
        self.executor = ThreadPoolExecutor( max_workers= 2)
    
    def handlePlayback (self, recorder):
        if recorder in self.recorderToFutureDict:
            if ( self.recorderToFutureDict[recorder].running()):
                self.cancelPlayback(recorder)
                                
        print ( self.handlePlayback.__name__, "registering playback in future" )
        self.recorderToFutureDict[recorder] = self.executor.submit( recorder.playback )
        
    def cancelPlaybackForController(self, midiEvent):
        # for each recorder, check if controller event is in that recorder, if yes, cancel the playback
        for recorder in self.recorderToFutureDict:
            if ( self.recorderToFutureDict[recorder].running()):
                if recorder.containsController( midiEvent ):
                    self.cancelPlayback(recorder)

    def cancelPlayback(self, recorder):
        print ("future is running, cancelling")
        recorder.stopPlayback()
        self.recorderToFutureDict[recorder].cancel()
        self.recorderToFutureDict[recorder].result()

class ControllerRecorder:

    def record ( self, midiEvent ):
        currentTime = currentTimeMilliseconds()
        
        timeFromPreviousEvent  = currentTime - self.previousEventTimestamp        
        
        if ( self.previousEventTimestamp == 0 ):
            timeFromPreviousEvent = 0
        
        self.recording.append({"event":midiEvent, "timestamp":timeFromPreviousEvent})
        
        self.previousEventTimestamp = currentTime
        #list of dictionaries with event, and time from previous event
        #take timestamp
        # take difference with previous timestamp
        # create dictiinary with midi event and timestamp
        # add it to list.
        # if first time for current recording. have timestamp as 0
        
    def playback(self):
        self.stopPlaybackFlag = False
        print (self.playback.__name__)
        for eventWithTimestamp in self.recording:
            if (self.stopPlaybackFlag == True ):
                return
            time.sleep(eventWithTimestamp["timestamp"]/1000)
            self.midiOut.sendMessage(eventWithTimestamp["event"])
    
    def stopPlayback(self):
        self.stopPlaybackFlag = True
        
    def containsController(self, midiEvent):
        for midiEventAndTimestamp in self.recording:
            if midiEventAndTimestamp["event"].getControllerNumber() == midiEvent.getControllerNumber():
                return True
        
    def __init__(self, midiOutParam):
        self.previousEventTimestamp = 0
        self.recording = list()
        self.stopPlaybackFlag = False
        self.midiOut = midiOutParam
    

class MidiEventsHandler:
    
    def __init__(self, midiOutParam):
        self.inControllerAssignMode = False
        self.inRecordControllerChangeMode = False
        self.lastControllerEvent = None
        self.noteToControllerMap = dict()
        self.noteToRecordingMap = dict()
        self.AssignControlleEventToKeyController = 27
        self.ClearMappings = 28
        # recording goes until noteon
        self.RecordControllerChange = 29
        self.playbackHandler = ControllerChangePlaybackHandler()
        self.midiOut = midiOutParam

    def callback( self,midiEvent ):
        try:
            print ("entering callback")
            if ( self.inControllerAssignMode == True and midiEvent.isNoteOn() ):
                print("in assignable mode")
                self.inControllerAssignMode = False
                print ("printing last controller event")
                print (self.lastControllerEvent)
                if (self.lastControllerEvent is not None ):
                    print("assigning note ", midiEvent.getNoteNumber(), " to controller ", self.lastControllerEvent.getControllerNumber() )                
                    self.noteToControllerMap[midiEvent.getNoteNumber()] = self.lastControllerEvent
                return
            elif ( self.inRecordControllerChangeMode == True ):
                if ( midiEvent.isController() ):
                    self.controllerRecorder.record(midiEvent)
                    self.midiOut.sendMessage(midiEvent)
                else:
                    self.inRecordControllerChangeMode = False                    
                    self.noteToRecordingMap[midiEvent.getNoteNumber()] = self.controllerRecorder                    
                    print ("assigning %d to playback recoring" % midiEvent.getNoteNumber() )
                return
                
            if ( midiEvent.isController() ):
                print("controller event")
                if midiEvent.getControllerNumber() == self.AssignControlleEventToKeyController:
                    print("Assign Controller event to keyboard")
                    self.inControllerAssignMode = True
                    return
                if midiEvent.getControllerNumber() == self.ClearMappings:
                    print("clearing mappings")
                    self.noteToControllerMap.clear()
                    self.noteToRecordingMap.clear()
                    return
                if midiEvent.getControllerNumber() == self.RecordControllerChange:
                    print("Record controller change")
                    self.inRecordControllerChangeMode = True
                    self.controllerRecorder = ControllerRecorder(self.midiOut)
                    return
                else:
                    print("assigning last controller event")
                    self.lastControllerEvent = midiEvent
                self.playbackHandler.cancelPlaybackForController(midiEvent)
                    
            #find the note in map, if yes, playback the controller event, return
            if ( midiEvent.getNoteNumber() in self.noteToControllerMap ):
                print("key %d is mapped to controlroller value" % midiEvent.getNoteNumber() )
                if ( midiEvent.isNoteOn() ):
                    self.midiOut.sendMessage(self.noteToControllerMap[midiEvent.getNoteNumber()])
                return
            
            if (midiEvent.getNoteNumber() in self.noteToRecordingMap ):
                print("key %d is mapped to controller recoring" % midiEvent.getNoteNumber() )
                if (midiEvent.isNoteOn()):
                    self.playbackHandler.handlePlayback(self.noteToRecordingMap[midiEvent.getNoteNumber()])                    
                return
                
            print("sending message")
            self.midiOut.sendMessage(midiEvent)
            print("printing event")
            print (midiEvent)
        except:
            print ( "Unexpected error:", sys.exc_info()[0] )
            print (traceback.format_exc()) 