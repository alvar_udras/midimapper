import Tkinter as tk
import ttk

def lb_select( evt ):    
    print "list box element selected %s" % evt.widget.get(evt.widget.curselection()[0])

class midiUtilityGUI(tk.Frame):    

    def connectClickedCallback(self):
        None

    def createWidgets(self):
        
        self.selectMidiInLbl = ttk.Label(self, text="Select midi input:").grid(row = 0, column = 0)
        
        self.midiInLbx = tk.Listbox ( self, exportselection=0 )
        self.midiInLbx.grid(row = 1, column = 0, rowspan = 5 )
        self.midiInLbx.bind('<<ListboxSelect>>', lb_select)
        
        self.selectMidiOutLbl = ttk.Label(self, text="Select midi output:").grid(row = 0, column = 1)
        
        self.midiOutLbx = tk.Listbox ( self, exportselection=0 )
        self.midiOutLbx.grid(row = 1, column = 1, rowspan = 5 )
        self.midiOutLbx.bind('<<ListboxSelect>>', lb_select)                     

        self.connectBtn = ttk.Button(self)
        self.connectBtn["text"] = "Connect",        
        self.connectBtn.grid(row = 6, column = 0)           
                
        self.statusLbl = ttk.Label(self)
        self.statusLbl.grid(row = 6, column = 1)
        
        self.selectKeyToControllerMapControllerLbl = ttk.Label(self, text="Controller To trigger key to controller assign mode").grid(row = 0, column = 2)
        
        self.triggerKeyToControllercb = ttk.Combobox(self)
        self.triggerKeyToControllercb.grid(row = 1, column = 2)
        self.triggerKeyToControllercb["values"] = range(1,127)
        
        self.selectKeyToControllerRecordingMapControllerLbl = ttk.Label(self, text="Controller To trigger controller recording").grid(row = 2, column = 2)
        
        self.triggerKeyToRecordingcb = ttk.Combobox(self)
        self.triggerKeyToRecordingcb.grid(row = 3, column = 2)
        self.triggerKeyToRecordingcb["values"] = range(1,127)

        self.resetMappingsTriggerLb = ttk.Label(self, text="reset controller").grid(row = 4, column = 2)

        self.resetMappingsTriggerCb = ttk.Combobox(self)
        self.resetMappingsTriggerCb.grid(row = 5, column = 2 )
        self.resetMappingsTriggerCb["values"] = range(1,127)

    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.pack()
        self.createWidgets()
        
    def setMidiInputValues(self, values):
        for i in range (values.getPortCount()):
            self.midiInLbx.insert(i, values.getPortName(i))
            
    def setMidiOutputValues(self, values):
        for i in range (values.getPortCount()):
            self.midiOutLbx.insert(i, values.getPortName(i))
            
    def setConnectHandler(self, callback):
        self.connectBtn["command"] = callback
        
    def getSelectedMidiOutput(self):
        return self.midiOutLbx.curselection()[0]
        
    def getSelectedMidiInput(self):
        return self.midiInLbx.curselection()[0]
    
    def setStatusMessage(self, message):
        self.statusLbl["text"] = message
        
    def getTriggerKeyToController(self):
        return self.triggerKeyToControllercb["values"][self.triggerKeyToControllercb.current()]
        
    def getTriggerKeyToRecording(self):
        return self.triggerKeyToRecordingcb["values"][self.triggerKeyToRecordingcb.current()]
        
    def getTriggerResetMappings(self):
        return self.resetMappingsTriggerCb["values"][self.resetMappingsTriggerCb.current()]