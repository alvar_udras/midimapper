import rtmidi
import sys
from pip._vendor.distlib.compat import raw_input
from midi_handlers import MidiEventsHandler
import Tkinter as tk
from midiUtilityGUI import midiUtilityGUI

root = tk.Tk()
midiGUI = midiUtilityGUI(master=root)
midiin = rtmidi.RtMidiIn()
midiout = rtmidi.RtMidiOut()

def handleConnect():
    midiInPort = midiGUI.getSelectedMidiInput()
    midiOutPort = midiGUI.getSelectedMidiOutput()
    midiin.closePort()
    midiout.closePort()
    midiin.cancelCallback()
    midiin.openPort(midiInPort)
    midiout.openPort(midiOutPort)
    midiEventsHandler = MidiEventsHandler(midiout)
    midiin.setCallback( midiEventsHandler.callback )
    if midiGUI.getTriggerKeyToController() > 0:
        midiEventsHandler.AssignControlleEventToKeyController = midiGUI.getTriggerKeyToController()
    if midiGUI.getTriggerKeyToRecording() > 0:
        midiEventsHandler.RecordControllerChange = midiGUI.getTriggerKeyToRecording()             
    if midiGUI.getTriggerResetMappings() > 0:
        midiEventsHandler.ClearMappings = midiGUI.getTriggerResetMappings()    
    midiGUI.setStatusMessage("connect ok")

if __name__ == '__main__':
    midiGUI.setMidiInputValues(midiin)
    midiGUI.setMidiOutputValues(midiout)
    
    midiGUI.setConnectHandler( handleConnect )
    
    midiGUI.mainloop()
    root.destroy()